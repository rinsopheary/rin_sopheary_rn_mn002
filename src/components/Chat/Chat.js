import React, {useState, useCallback, useEffect} from 'react';
import {GiftedChat} from 'react-native-gifted-chat';
import {senderMsg, recieverMsg} from '../../network';
import firebase from '../../firebase/config';
import {senderMessage, receiverMessage} from '../../network/messeges';
export default function Chat({route, navigation}) {
  const [messages, setMessages] = useState([]);
  const {params} = route;
  const {name, img, imgText, receiverId, senderId} = params;

  useEffect(() => {
    try {
      firebase
        .database()
        .ref('chats/')
        .child(senderId)
        .child(receiverId)
        .on('value', (dataSnapshot) => {
          console.log('work');
          let msgs = [];
          setMessages([]);
          dataSnapshot.forEach((child) => {
            // console.log(child.val());
            var data = child.val().data;
            console.log(data)
            msgs.push({
              _id: data._id,
              text: data.text,
              createdAt: new Date(data.createdAt),
              user: data.user,
            });
          });

          console.log(msgs)
          setMessages(msgs.reverse())
        });
        
    } catch (error) {
      console.log(error);
    }
  }, []);

  const onSend = useCallback((messages = []) => {
    var msg = messages[0].text
    senderMessage(msg, senderId, receiverId)
      .then(() => {
        console.log('SENDER');
      })
      .catch((error) => console.log(error));
    receiverMessage(msg, senderId, receiverId)
      .then(() => {
        console.log("RECEIVER")
      })
    //   .catch((error) => console.log(error));
  }, []);

  return (
    <GiftedChat
      messages={messages}
      onSend={(messages) => onSend(messages)}
      user={{
        _id: senderId,
      }}
    />
  );
}
