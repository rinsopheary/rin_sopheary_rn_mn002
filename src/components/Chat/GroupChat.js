import { Button } from 'native-base'
import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import { TextInput } from 'react-native-gesture-handler'
import {groupChat} from '../../network/messeges'

export default class GroupChat extends Component {
    handlerCreate = async ()=>{
        // alert('hi')
        groupChat('My Group').then(()=>{

        }).catch((err)=>console.log(err))
    }
    render() {
        return (
            <View style ={{padding : 15}}>
                <Text style={styles.textStyle}> Create Room </Text>
                <TextInput style={{ borderWidth: 1, padding: 10 }}></TextInput>
                <Button block primary style={styles.buttonStyle}
                    onPress = {()=> this.handlerCreate()}
                >
                    <Text>Create</Text>
                </Button>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    textStyle : {
        fontSize : 20,
        fontWeight : "bold",
        marginBottom : 20
    },
    buttonStyle : {
        marginTop: 10
    }
})
