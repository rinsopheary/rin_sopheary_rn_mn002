import React, {useState, useCallback, useEffect} from 'react';
import {GiftedChat} from 'react-native-gifted-chat';
import {senderMsg, recieverMsg} from '../../network';
import firebase from '../../firebase/config';
import {sendChatToGroupChat} from '../../network/messeges';
export default function GroupChatting({route, navigation}) {
  const [messages, setMessages] = useState([]);
  const {params} = route;
  const {name, img, imgText, groupId, senderId} = params;

  useEffect(() => {
    try {
      firebase
        .database()
        .ref('bot_groups/')
        .child(groupId)
        .child('messages')
        .on('value', (dataSnapshot) => {
        //   console.log('work');
        //   let msgs = [];
        //   setMessages([]);
          dataSnapshot.forEach((child) => {
            var data = child.val().data;
            console.log(data)
            msgs.push({
              _id: data._id,
              text: data.text,
              createdAt: new Date(data.createdAt),
              user: data.user,
            });
          });

          console.log(msgs)
          setMessages(msgs.reverse())
        });
        
    } catch (error) {
      console.log(error);
    }
  }, []);

  const onSend = useCallback((messages = []) => {
      console.log(messages)
    sendChatToGroupChat(groupId, senderId, 'msg').then(()=>{
        console.log("SENDER")
    }).catch((err)=>console.log(err))
    //   .catch((error) => console.log(error));
  }, []);

  return (
    <GiftedChat
      messages={messages}
      onSend={(messages) => onSend(messages)}
      user={{
        _id: senderId,
      }}
    />
  );
}
