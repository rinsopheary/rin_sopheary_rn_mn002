import React, { Component } from 'react'
import { StyleSheet, View, FlatList, Image, TouchableOpacity } from 'react-native'
import { Container, Header, Input, Content, Item, Footer, FooterTab, Button, Text, Right, Card, Left } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { getUser, getUsers } from '../../network/user'
import { getGroupChat } from '../../network/messeges';

export default class HomeScreen extends Component {
    constructor(props) {
        super(props)
        console.log()
        this.state = {
            myId: props.route.params.myId,
            user: {},
            users: [],
            groups: []
        }
    }

    componentDidMount() {
        // console.log('props values =======>', this.props);
        // console.log(this.state.myId)
        // console.log(this.state.username);
        console.log();
        getUser(this.state.myId).then((res) => {
            console.log(res.val())
            this.setState({
                user: res.val()
            })

        }).catch((error) => {
            console.log(error);
        })

        getUsers().then((res) => {
            var u = []
            res.forEach(user => {
                console.log(user.val())
                u.push({ id: user.val().uuid, username: user.val().name })
            });
            this.setState({
                users: u,
            })
            // console.log(res.val())
            // this.setState({data: res.val()})
            // console.log(this.state.data)
        })
    }

    render() {
        return (
            <Container>
                <Header
                    style={{ backgroundColor: "#34626c" }}
                >
                    <Left>
                        <Button
                            onPress={() =>
                                this.props.navigation.navigate('User Profile', {
                                    user: this.state.user
                                })

                            }
                            transparent>

                            <Icon
                                name="user-circle"
                                style={styles.iconStyle}
                            />
                            <Text style={{ fontSize: 30, color: 'white', marginLeft: 10, fontWeight: "bold" }}>Chat</Text>
                        </Button>
                    </Left>
                    <Right>
                        <Button
                            onPress={() =>
                                this.props.navigation.navigate('Group Chat', {
                                    user: this.state.user
                                })

                            }
                            transparent>
                            <Icon
                                name="users"
                                style={styles.iconStyle}
                            />
                        </Button>
                    </Right>
                </Header>

                <Content>
                    <FlatList
                        data={this.state.users}
                        keyExtractor={item => item.id}
                        renderItem={({ item }) => (
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate("Chat", {
                                    name: "",
                                    img: "profileImg",
                                    receiverId: item.id,
                                    senderId: this.state.myId
                                })}
                            >
                                <Card

                                >
                                    <View style={{ flexDirection: "row", justifyContent: "space-between", padding: 15 }}>
                                        <View style={{ width: '20%' }}>
                                            <Image source={{ uri: 'https://img2.pngio.com/default-image-png-picture-710225-default-image-png-default-png-376_356.png' }}
                                                style={{ width: 35, height: 35 }}
                                            />
                                        </View>
                                        <View style={{ width: '80%' }}>
                                            <Text numberOfLines={2}
                                                style={{ fontSize: 20, fontWeight: "bold", marginBottom: 12 }}
                                            >{item.username}</Text>

                                        </View>

                                    </View>
                                </Card>
                            </TouchableOpacity>
                        )}
                    >

                    </FlatList>
                </Content>
            </Container>

        )
    }
}

const styles = StyleSheet.create({
    iconStyle: {
        fontSize: 30,
        color: 'white'
    }
})
