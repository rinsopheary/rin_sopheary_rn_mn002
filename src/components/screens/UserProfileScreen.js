import React, { Component } from 'react'
import { Text, View , Image} from 'react-native'

export class UserProfileScreen extends Component {
    constructor(props) {
        super(props)
        console.log(this.props.route.params.user);
        this.state = {
            user : this.props.route.params.user
        }
    }
    
    render() {
        return (
            <View style={{flex: 1,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',}}>
                <Image source={{ uri: 'https://img2.pngio.com/default-image-png-picture-710225-default-image-png-default-png-376_356.png' }}
                    style={{ width: 35, height: 35 }}
                />
                <Text>{this.state.user.name}</Text>
                <Text>{this.state.user.email}</Text>
            </View>
        )
    }
}

export default UserProfileScreen
