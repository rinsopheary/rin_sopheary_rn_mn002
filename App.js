import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
// import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from './src/components/screens/LoginScreen';
import SignupScreen from './src/components/screens/SignupScreen';
import HomeScreen from './src/components/homepage/HomeScreen';
import Chat from './src/components/Chat/Chat';
import UserProfileScreen from './src/components/screens/UserProfileScreen';
import GroupChat from './src/components/Chat/GroupChat';
// import GroupChatting from './src/components/Chat/GroupChatting';

const Stack = createStackNavigator();

export default class App extends Component {

  render() {
    return (
      <NavigationContainer>
    <Stack.Navigator>

       <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{ headerShown: null }}
        />

        <Stack.Screen
          name="Signup"
          component={SignupScreen}
          options={{ headerShown: null }}
        />

        <Stack.Screen
          name="HomePage"
          component={HomeScreen}
          options={{ headerShown: null}}
        />

        <Stack.Screen
          name="Chat"
          component={Chat}
          options={{ headerShown: true,title: 'My Chat' }}
        />

        <Stack.Screen
          name="User Profile"
          component={UserProfileScreen}
          options={{ headerShown: true}}
        />

        <Stack.Screen
          name="Group Chat"
          component={GroupChat}
          options={{ headerShown: true, title : 'Add Group'}}
        />

      {/* <Stack.Screen
          name="Group Chatting"
          component={GroupChatting}
          options={{ headerShown: true, title : 'Group Chat'}}
        /> */}

      </Stack.Navigator>
    </NavigationContainer>
    )
  }
}

const styles = StyleSheet.create({})
